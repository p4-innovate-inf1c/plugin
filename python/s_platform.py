import sqlite3
import logging
import json

from paho.mqtt import client as mqtt_client

auth = open('/etc/PowerPot/config.json')
data = json.load(auth)

manufacturer = 'PowerPot B.V.'
model = 'Smart Platforms'
version = '2022-06-08'
expire_time = 30

mqtt_host = data.get('mqttIp')
mqtt_port = 1883
mqtt_user = data.get('username')
mqtt_pass = data.get('password')
db_file = '/etc/PowerPot/powerpot.db'

# Set the logging params
logging.basicConfig(format='[MQTT-Manager] %(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logger = logging.getLogger()
logger.setLevel("INFO")


def send_mqtt(client, topic, payload, log=True):
    result = client.publish(topic, payload)
    status = result[0]
    if status == 0:
        if log:
            logger.debug(f'Send to topic `{topic}`: {payload}')
    else:
        logger.error(f'Failed to send message to topic {topic}')


def get_sensors(friendly_name):
    return [
        {
            'name': f'plant_name',
            'friendly_name': f'{friendly_name}: plant name',
        }, {
            'name': f'nfc_id',
            'friendly_name': f'{friendly_name}: nfc id',
        }, {
            'name': f'ldr_sensor',
            'friendly_name': f'{friendly_name}: ldr sensor',
        }, {
            'name': f'ldr_advice',
            'friendly_name': f'{friendly_name}: ldr advice',
        }, {
            'name': f'moisture',
            'friendly_name': f'{friendly_name}: moisture',
        }, {
            'name': f'moisture_advice',
            'friendly_name': f'{friendly_name}: moisture advice',
        },
    ]


class PlatformManager:

    def __init__(self):
        self.platforms = {}
        self.__client = mqtt_client.Client(f'PowerPot-MQTT-Manager')
        self.__client.username_pw_set(mqtt_user, mqtt_pass)
        self.__client.on_connect = self.handle_connect
        try:
            self.__client.connect(mqtt_host, mqtt_port)
        except TimeoutError:
            # Connection has timed out
            logger.error("Timeout while connecting to the mqtt server")
            exit()
        self.__client.on_message = self.handle_mqtt
        self.__client.loop_forever()

    def handle_connect(self, client, userdata, flags, rc):
        if rc == 0:
            logger.info(f'Connected to MQTT {mqtt_host}:{mqtt_port} as {mqtt_user}')
            # On connect subscribe to messages
            self.subscribe('homeassistant/sensor/+/linkquality/config')
        else:
            logger.error(f"Failed to connect to the MQTT {mqtt_host}:{mqtt_port} as {mqtt_user}, return code {rc}")

    def handle_mqtt(self, client, userdata, msg):
        try:
            data = json.loads(msg.payload.decode())
        except json.decoder.JSONDecodeError:
            # JSON data cannot be parsed
            return
        except TypeError:
            logger.info("JSON: non string recieved")
            logger.info(msg.payload.decode())
            logger.info(type(msg.payload.decode()))
            return
        topic = msg.topic
        if "homeassistant/sensor/" in topic:
            device = data.get('device')
            if 'PowerPot.sensor' in device.get('model') and device.get('manufacturer') == 'PowerPot B.V.':

                ieee = device.get('identifiers')[0].split("_")[-1]
                old_data = None
                for platform in self.platforms.keys():
                    if self.platforms[platform].get('ieee') == ieee:
                        old_data = self.platforms[platform]
                        break
                if old_data:
                    friendly_name = old_data.get('friendly_name')
                    if friendly_name:
                        self.unsubscribe('zigbee2mqtt/' + friendly_name)
                friendly_name = device.get('name')
                self.platforms[friendly_name] = {
                    'ieee': ieee,
                    'pieee': f'{ieee}p',
                    'friendly_name': friendly_name,
                    'sensors': get_sensors(friendly_name),
                    'error_count': 0,
                    'sensor_values': {}
                }
                self.subscribe('zigbee2mqtt/' + friendly_name)
                self.register_sensor(friendly_name)
            return
        if "zigbee2mqtt/" in topic:
            friendly_name = topic.split('/')[-1]
            self.update_sensors(friendly_name, data)

    def update_sensors(self, friendly_name, data):
        platform = self.platforms[friendly_name]

        try:
            action = json.loads(data.get('action'))
        except json.decoder.JSONDecodeError:
            platform['error_count'] += 1
            if platform['error_count'] > 1:
                platform['sensor_values'] = {}
            self.send_sensors(platform)
            return
        except TypeError:
            logger.debug("JSON: non string recieved")
            action = None
        if action:
            platform['error_count'] = 0
            nfc_id = action.get('status')
            if nfc_id:
                platform['sensor_values'] = {'nfc_id': nfc_id}
                self.get_data_from_db(friendly_name, nfc_id)
        platform['sensor_values']['ldr_sensor'] = data.get('l1')
        platform['sensor_values']['moisture'] = data.get('l4')
        self.send_sensors(platform)

    def subscribe(self, topic):
        logger.info(f'Subscribed to topic "{topic}"')
        self.__client.subscribe(topic)

    def unsubscribe(self, topic):
        logger.info(f'Unsubscribe to topic "{topic}"')
        self.__client.unsubscribe(topic)

    def register_sensor(self, friendly_name):
        platform = self.platforms.get(friendly_name)
        if platform:
            pieee = platform.get('pieee')
            friendly_name = platform.get('friendly_name')
            for sensor in platform.get('sensors'):
                data = json.dumps({
                    "availability": [
                        {
                            "topic": f"PowerPot/{pieee}/availability",
                            "value_template": "{{ value_json.state }}"
                        }
                    ],
                    "device": {
                        "identifiers": [pieee],
                        "manufacturer": manufacturer,
                        "model": model,
                        "name": f"Platform-{friendly_name}",
                        "unique_id": pieee,
                        "sw_version": version
                    },
                    "name": friendly_name + '_' + sensor.get("name"),
                    "expire_after": expire_time,
                    "entity_category": "diagnostic",
                    "state_topic": f"PowerPot/{pieee}",
                    "unique_id": friendly_name + sensor.get("name"),
                    "value_template": '{{ value_json.' + sensor.get("name") + ' }}'
                })
                send_mqtt(self.__client, f'homeassistant/sensor/{pieee}/{sensor.get("name")}/config', data)
            send_mqtt(self.__client, f"PowerPot/{pieee}/availability", json.dumps({
                "state": "online"
            }), False)

    def send_sensors(self, platform):
        pieee = platform.get('pieee')
        send_mqtt(self.__client, f"PowerPot/{pieee}", json.dumps({
            "plant_name": platform['sensor_values'].get('plant_name', 'onbekend'),
            "nfc_id": platform['sensor_values'].get('nfc_id'),
            "ldr_sensor": platform['sensor_values'].get('ldr_sensor', 0),
            "ldr_advice": platform['sensor_values'].get('ldr_advice', 0),
            "moisture": platform['sensor_values'].get('moisture', 0),
            "moisture_advice": platform['sensor_values'].get('moisture_advice', 0),
        }))
        send_mqtt(self.__client, f"PowerPot/{pieee}/availability", json.dumps({
            "state": "online"
        }), False)

    def get_data_from_db(self, friendly_name, nfc_id):
        con = sqlite3.connect(db_file)
        cur = con.cursor()
        rows = cur.execute(
            'SELECT plant.name, plant.moisture, plant.optimal_light FROM pot JOIN plant on pot.plant_id=plant.id WHERE nfc_id="%s" LIMIT 1' % nfc_id)
        for row in rows:
            self.platforms[friendly_name]['sensor_values']['plant_name'] = row[0]
            self.platforms[friendly_name]['sensor_values']['moisture_advice'] = row[1]
            self.platforms[friendly_name]['sensor_values']['ldr_advice'] = row[2]


# Start the platformManager
my_platform = PlatformManager()
