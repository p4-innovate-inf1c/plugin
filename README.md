# Powerpot addon

## **About**

This plugin is used for a product called "PowerPot". PowerPot is a product which consists of a plant pot and a smart
platform.
The platform has sensors built-in and when you put a plant pot on the platform, this plugin will show the specifications
of the plant. Specifications
like soil moisture and light intensity. This way you can keep track on the status of your plant.

## **Configurations**

To configurate you have to go to your webbrowser and fill in:

1. Your IP-adress, you'll end at the login page
2. Enter a username
3. Enter a password
4. Log in

These are the credentials of Home Assistant and Mqtt.
You can adjust these anytime by clicking on the setting gear wheel icon above in the plugin.

## **Requirements**

To use this plugin, you'll need to install other add-ons.
The plugins you have to install are:

1. Zigbee2MQTT: https://github.com/zigbee2mqtt/hassio-zigbee2mqtt#installation
2. Mosquitto MQTT broker: https://github.com/home-assistant/addons/blob/master/mosquitto/DOCS.md

Without those two add-ons, this plugin will not work.

## **How to install the add-on**

1. Log into Home Assistant.
2. Click on the "Add Repository". This will add the plugin in the list with add-ons.
3. Go to settings then Add-ons, there you will see "PowerPot" in the list. Click on it and install it.

[![Open your Home Assistant instance and show the add add-on repository dialog with a specific repository URL pre-filled.](https://my.home-assistant.io/badges/supervisor_add_addon_repository.svg)](https://my.home-assistant.io/redirect/supervisor_add_addon_repository/?repository_url=https%3A%2F%2Fgitlab.com%2Fp4-innovate-inf1c%2Fplugin)

## **How to use the add-on**

1. To use this plugin you'll need to install the two other add-ons that have been descriped above.
2. After installing turn on your platform through the powerbank.
3. Go back to zigbee2mqtt and click on "allow login".
4. Go to the PowerPot plugin and there you will see the platform.

## Local testing (Docker)

To test this plugin locally, you will have to build your image. You can do this by going to the directory where your
clone resides (where the Dockerfile resides) and execute the following commands

**Building the image**
`docker build -t powerpot .`

**Showing your container**

`docker image ls`

**Starting your container**

`docker run -p 8080:8080 -d powerpot`

## Local testing (Docker-Compose)
For live editing testing you need to uncomment:
```yaml
volumes:
  - ./www:/var/www/html
```
**Warning:** this can result in errors in the file uploads because of rights


**Building the image**
```shell
docker-compose build
```

**Starting your container**
```shell
docker-compose up
```