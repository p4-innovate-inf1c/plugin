<?php
require_once 'database.php';

$db = connect_db();
$error = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['id'])) {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
    $moisture = filter_input(INPUT_POST, 'moisture', FILTER_SANITIZE_SPECIAL_CHARS);
    $optimal_light = filter_input(INPUT_POST, 'optimal_light', FILTER_SANITIZE_SPECIAL_CHARS);
    $edit_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);

    if (!empty($name) && !empty($moisture) && !empty($optimal_light) && !empty($edit_id)) {
        if (!is_numeric($moisture) && !is_numeric($optimal_light)) {
            $error = "Please make sure that moisture and optimal light are numbers";
        } else {
            $stmt = $db->prepare('UPDATE plant SET name=:name, moisture=:moisture, optimal_light=:optimal_light WHERE id=:id');
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':moisture', $moisture, PDO::PARAM_INT);
            $stmt->bindParam(':optimal_light', $optimal_light, PDO::PARAM_INT);
            $stmt->bindParam(':id', $edit_id, PDO::PARAM_INT);
            $stmt->execute();
            header("Location:./plant_overview.php");
            exit();
        }
    } else {
        $error = "Please fill in all fields!";
    }
}

if (isset($_GET['id'])) {
    $stmt = $db->prepare('SELECT * FROM plant WHERE id = :id');
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);

    $stmt->execute();
    $plant = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

if (empty($plant)) {
    header('Location:./create_plant.php');
    exit();
}

$plant = $plant[0];
$plant_name = htmlspecialchars($plant['name']);
$plant_id = htmlspecialchars($plant['id']);
$plant_moisture = htmlspecialchars($plant['moisture']);
$plant_optimal_light = htmlspecialchars($plant['optimal_light']);
$plant_image = htmlspecialchars($plant['image_path']);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="resources/css/template.css" rel="stylesheet">
    <title>Manage <?= $plant_name ?></title>
</head>

<body>
<?php require_once 'header.php'; ?>
<h1 class="site_header">Edit Plant</h1>
<main>
    <div class="card">
        <img class="plant-image" src="image/<?= $plant_image ?>" alt="plant-image">
        <form action="manage_plant.php?id=<?= $plant_id ?>" method="POST">
            <label for="name">Name plant:</label>
            <input type="text" name="name" value="<?= $plant_name ?>" id="name">
            <label for="moisture">Max moisture:</label>
            <input type="text" name="moisture" value="<?= $plant_moisture ?>" id="moisture">
            <label for="optimal_light">Optimal light:</label>
            <input type="text" name="optimal_light" value="<?= $plant_optimal_light ?>" id="optimal_light">
            <input type="submit" value="Save">
        </form>
        <div class="error-message">
            <?php if (isset($error)) { ?>
                <p>
                    <?= $error; ?>
                </p>
                <?php
            } else if (isset($uploadError)) { ?>
                <p>
                    <?= $uploadError; ?>
                </p>
            <?php } ?>
        </div>
    </div>
</main>
</body>

</html>
