<?php
$msg = "";

$config = new stdClass();
$config->username = "";
$config->password = "";
$config->mqttIp = "";

if (file_exists('/etc/PowerPot/config.json')) {
  try{
    $filedata = file_get_contents('/etc/PowerPot/config.json');

    } catch (Exception) {
        $msg = "Cant open file";
        die();
    }
    $json = json_decode($filedata);
    if (json_last_error() == 0) {
        $config = $json;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $username = $_POST["username"];
        $password = $_POST["password"];
        $mqttIp   = $_POST["mqttIp"];

        if (!empty($username) && !empty($password) && !empty($mqttIp)) {
            if(filter_var($mqttIp, FILTER_VALIDATE_IP)){


              $config->username = $username;
              $config->password = $password;
              $config->mqttIp = $mqttIp;
              $new_config = json_encode($config, JSON_PRETTY_PRINT);
              if (file_put_contents('/etc/PowerPot/config.json', $new_config)) {
                  $msg = "Data successfully updated";
              } else {
                  $msg = "Something went wrong";
              }
            }else{
              $msg = "Not a valid ip address";
            }
        } else {
            $msg = "Please fill all the field";
        }
    }
} else {
    $msg = "JSON file does not exists";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Config</title>
    <link href="resources/css/template.css" rel="stylesheet">
</head>

<body>
<?php require_once 'header.php'; ?>
<h1 class="site_header">Credentials</h1>
<form action="config.php" method="POST" class="card">
    <?php if (!empty($msg)) { ?>
        <p class="msg"><?= $msg ?></p>
    <?php } ?>
    <label>Username: <input type="text" id="username" name="username"
                            value="<?= $config->username ?>"></label>
    <label>Password: <input type="text" id="password" name="password"
                            value="<?= $config->password ?>"></label>
    <label>Ip-Adress: <input type="text" id="mqttIp" name="mqttIp"
                            value="<?= $config->mqttIp ?>"></label>

    <input type="submit" class="button" value="Update credentials">
</form>
</body>

</html>
