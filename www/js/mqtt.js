// All data will be stored in the entityData variable ([0[x]] = first entity from powerpot which is online, [1[x]] is the second entity etc)
// [0[0]] = entity_id
// [0[1]] = friendly name
// [0[2]] = sensor values (l1, l4 etc.)
let entityUpdates = [];
let entities = [];
let currentEntities = [];
let nfcId = null;
let plantPotDb;


// Create a client instance: Broker, Port, Client ID
client = new Paho.MQTT.Client(MqttIp, 1884, `Powerpot-${Date.now(), Math.random()}`);

// set callback handlers
client.onConnectionLost = function (responseObject) {
  console.log("Connection Lost: " + responseObject.errorMessage);
}

client.onMessageArrived = function (message) {
  let friendlyNameZ2M = message.destinationName.split("/"); // used in z2mqtt path
  if (message.payloadString != "") {
    let jsonData = JSON.parse(message.payloadString);

    if (jsonData.device != null) {
      if (jsonData.device.manufacturer == "PowerPot B.V." && jsonData.device.model.includes('PowerPot.sensor')) {
        entityId = jsonData.device.identifiers[0].split("_")[1];
        if (currentEntities.indexOf(entityId) === -1) {
          if(entityUpdates.length === 0){
            document.getElementById("overview").innerText="";
          }
          entityUpdates.push([entityId, jsonData.device.name]); // push the entity_id & the friendly name in the entityData 2d array
          entities.push([entityId, jsonData.device.name])

          generateEntity(entities[0]);
          currentEntities.push(entityId);
        } else {
          entityUpdates.push([entityId, jsonData.device.name]);
        }

      }
    } else { // used for updates on sensor values.
      entityUpdates.forEach(tmpEntity => {
        if (friendlyNameZ2M.length == 2 && tmpEntity[1] == friendlyNameZ2M[1]) {
          if (tmpEntity.length == 2) {
            tmpEntity.push(jsonData);
          }
          else if (tmpEntity.length == 3) {
            tmpEntity.pop();
            tmpEntity.push(jsonData);
          }
          updateEntity(tmpEntity);
        }
      });
    }
  }
}

// Called when the connection is made
function onConnect() {
  client.subscribe("homeassistant/sensor/+/linkquality/config"); // this path returns all sensors that are present in homeassistant
  client.subscribe("zigbee2mqtt/+"); // this path returns all sensor values from a specific entity (by friendly name)
  document.getElementById("overview").innerHTML = "<h2>no devices</h2>";
}

// Connect the client, providing an onConnect callback
client.connect({
  onSuccess: onConnect,
  userName: Username,
  password: Password,
});

// generateEntities() :
function generateEntity(currentEntity) {
  document.getElementById("overview").innerHTML += `
    <div class='item_container'>
      <div class='name_container'>
        <p id='name_${currentEntity[0]}' class='platform_name'> ${currentEntity[1]} </p>
      </div>

      <div id="platform_${currentEntity[0]}" class='platform_data'> No pot detected! </div>
      <div class='image_container'>
        <img id="image_${currentEntity[0]}" class="platform_image" src="image/logo.png" alt="image">
      </div>
      <div class='platform_data'>
        <p class='platform_name'> ${currentEntity[0]} </p>
      </div>
      <div class='platform_data'>
        <p id='moisture_${currentEntity[0]}' class='platform_data_text'> - </p>
        <p id='light_${currentEntity[0]}' class='platform_data_text'> - </p>
      </div>
      <a id='link_${currentEntity[0]}' href='#' class='button'> No pot detected! </a>
    </div>
  `
  entities.pop();
};

// on every interval from home assistant, update the entities that are present
function updateEntity(currentEntity) {
  document.getElementById(`name_${currentEntity[0]}`).innerHTML = currentEntity[1]
  try {
    nfcId = JSON.parse(currentEntity[2].action);
  } catch {
    if (nfcId != null && nfcId.hasOwnProperty('status') && currentEntity != null) {
      // return the specific record from the database, filtered on the nfc_id
      getApiData(`/api/pot.php?nfc_id=${nfcId.status}`).then(data => { plantPotDb = data });
      if (plantPotDb != undefined && !plantPotDb.hasOwnProperty("ERROR")) {
        potPlantDetected(currentEntity, plantPotDb);
      } else if (plantPotDb != undefined) {
        potDetected(currentEntity);
      }
    } else {
      plantPotDb = undefined;
      noPotDetected(currentEntity);
    }
  }
}

// returns the data from the /api endpoint
async function getApiData(url) {
  obj = await (await fetch(url)).json();
  return obj;
}

// no pot is detected, only the platform is online
function noPotDetected(data) {
  document.getElementById(`light_${data[0]}`).innerHTML = "Light: N/A";
  document.getElementById(`moisture_${data[0]}`).innerHTML = "Moisture: N/A";
  document.getElementById(`link_${data[0]}`).href = "#";
  document.getElementById(`link_${data[0]}`).innerHTML = "No pot detected!";
  document.getElementById(`platform_${data[0]}`).innerHTML = "No pot detected!";
  document.getElementById(`image_${data[0]}`).src = `image/logo.png`;
}

// a pot is detected, but it does have a record that is written in the database
function potDetected(data) {
  document.getElementById(`image_${data[0]}`).src = `image/logo.png`;
  document.getElementById(`light_${data[0]}`).innerHTML = `Light: N/A`;
  document.getElementById(`moisture_${data[0]}`).innerHTML = `Moisture: N/A`;
  document.getElementById(`platform_${data[0]}`).innerHTML = `Pot detected  <strong>: ${nfcId.status}</strong>`;
  document.getElementById(`link_${data[0]}`).href = `manage_pot.php?nfc_id=${nfcId.status}`;
  document.getElementById(`link_${data[0]}`).innerHTML = `Edit`;
}

// a pot is detected, and it has a record that is known within the database
function potPlantDetected(data, dbdata) {
  if (dbdata != null) {
    document.getElementById(`image_${data[0]}`).src = `image/${dbdata.image_path}`;
    document.getElementById(`light_${data[0]}`).innerHTML = `Light: ${convertToPercentage(data[2].l1, 2000)}% / ${dbdata.optimal_light}%`;
    document.getElementById(`moisture_${data[0]}`).innerHTML = `Moisture: ${convertToPercentage(data[2].l4, 2000)}% / ${dbdata.moisture}%`;
    document.getElementById(`link_${data[0]}`).href = `manage_pot.php?nfc_id=${nfcId.status}`;
    document.getElementById(`link_${data[0]}`).innerHTML = `Edit`;
    document.getElementById(`platform_${data[0]}`).innerHTML = `Plant detected  <strong>: ${dbdata.name}</strong>`;
  }
}

// convert a sensor value to a percentage
function convertToPercentage(sensorValue, maxSensorValue){
  return  sensorValue / (maxSensorValue / 100);
}
