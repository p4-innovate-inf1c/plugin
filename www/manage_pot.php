<?php
require_once 'database.php';
$db = connect_db();

if (isset($_GET['nfc_id'])) {
    $stmt = $db->prepare(
        'SELECT *
                FROM pot
                LEFT JOIN plant
                on pot.plant_id = plant.id
                WHERE nfc_id LIKE :nfc_id
                LIMIT 1'
    );

    $nfc_id = filter_input(INPUT_GET, 'nfc_id', FILTER_SANITIZE_SPECIAL_CHARS);
    $stmt->bindValue(':nfc_id', $nfc_id);
    $stmt->execute();
    $associatedPlant = $stmt->fetch(PDO::FETCH_ASSOC);
}

if (isset($_POST['nfc_id']) && isset($_POST['plant_id'])) {
    $nfc_id = filter_input(INPUT_POST, 'nfc_id', FILTER_SANITIZE_SPECIAL_CHARS);
    $plant_id = filter_input(INPUT_POST, 'plant_id', FILTER_SANITIZE_SPECIAL_CHARS);
  
    if (isset($_POST['submit-insert'])) {
        $stmt = $db->prepare('INSERT INTO pot(nfc_id, plant_id) VALUES(:nfc_id, :plant_id)');
    } else if (isset($_POST['submit-update'])) {
        $stmt = $db->prepare('UPDATE pot SET plant_id=:plant_id WHERE nfc_id LIKE :nfc_id');
    }
    try {
        $stmt->bindParam(':nfc_id', $nfc_id);
        $stmt->bindParam(':plant_id', $plant_id, PDO::PARAM_INT);
        $stmt->execute();
    } catch (Exception $e) {
        exit("Unable to execute query, please check if your ID and/or name are unique : $e");
    }
    header('Location: index.php');
}

$stmt = $db->query('SELECT * FROM plant');
$plants = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="resources/css/template.css" rel="stylesheet">
    <title>Manage pot</title>
</head>

<body>
<?php require_once 'header.php'; ?>
<main>
    <div class="card">
        <?php if (isset($_GET['nfc_id'])) { ?>
            <?php if (isset($associatedPlant) && !empty($associatedPlant)) { ?>
                <img class="pot-plant-image" src="<?= "image/" . $associatedPlant["image_path"] ?>" alt="plant_image">
                <form action="manage_pot.php" method="post">
                    <label> Pot ID <input type="text" name="nfc_id"
                                          value="<?= htmlspecialchars($associatedPlant["nfc_id"]) ?>"> </label>
                    <label> Current plant
                        <select name="plant_id" id="plant_id">
                            <?php for ($plant = 0; $plant < count($plants); $plant++) {
                                $cur_plant = $plants[$plant];
                                $plant_id = htmlspecialchars($cur_plant["id"]);
                                $plant_name = htmlspecialchars($cur_plant["name"]);
                                if ($cur_plant["id"] == $associatedPlant["id"]) { ?>
                                    <option selected value="<?= $plant_id ?>"
                                            id="<?= $plant_id ?>"><?= $plant_name ?></option>
                                <?php } else { ?>
                                    <option value="<?= $plant_id ?>"
                                            id="<?= $plant_id ?>"><?= $plant_name ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </label>
                    <input type="submit" name="submit-update" value="Update">
                </form>
            <?php } else { ?>
                <img class="pot-plant-image" src="image/logo.png" alt="Plant image">
                <form action="manage_pot.php" method="post">
                    <label>Pot ID<input type="text" name="nfc_id" value="<?= htmlspecialchars($_GET['nfc_id']); ?>" readonly>
                    </label>
                    <label>Choose plant in pot
                        <select name="plant_id" id="plant_id">
                            <?php for ($plant = 0; $plant < count($plants); $plant++) { ?>
                                <option value="<?= htmlspecialchars($plants[$plant]["id"]) ?>"
                                        id="<?= htmlspecialchars($plants[$plant]["id"]) ?>"><?= htmlspecialchars($plants[$plant]["name"]) ?>
                                </option>
                            <?php } ?>
                        </select>
                    </label>
                    <input type="submit" name="submit-insert" value="Create">
                </form>
            <?php } ?>
        <?php } ?>
    </div>
</main>
</body>

</html>
