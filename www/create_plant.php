<?php
require_once 'database.php';

$db = connect_db();
$error = "";

function clean_filename($string): string
{
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9.\-]/', '', $string); // Removes special chars.
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
    $moisture = filter_input(INPUT_POST, 'moisture', FILTER_VALIDATE_INT);
    $optimal_light = filter_input(INPUT_POST, 'optimal_light', FILTER_VALIDATE_INT);
    $filename = clean_filename($_FILES["image"]["name"]);

    if (!empty($filename) && !empty($name)) {
        if (empty($optimal_light) || empty($moisture)) {
            $error = "Fill in a valid number for moisture and light";
        }
        require_once 'upload_image.php';
        if (empty($error)) {
            $query = 'INSERT INTO plant (name, moisture, optimal_light, image_path)
                      VALUES(:name, :moisture, :optimal_light, :image_path)';
            $stmt = $db->prepare($query);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':moisture', $moisture, PDO::PARAM_INT);
            $stmt->bindParam(':optimal_light', $optimal_light, PDO::PARAM_INT);
            $stmt->bindParam(':image_path', $filename);
            $stmt->execute();
            header("Location:./plant_overview.php");
            exit();
        }
    } else {
        $error = "Please fill in all fields!";
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="resources/css/template.css" rel="stylesheet">
    <link href="resources/css/tooltip.css" rel="stylesheet">
    <title>Create Plant</title>
</head>

<body>
    <?php require_once 'header.php'; ?>
    <main>
        <h1 class="site_header">Create plant</h1>
        <form action="create_plant.php" method="post" enctype="multipart/form-data" class="card">

            <label>Name plant <input type="text" name="name"></label>

            <label>Optimal moisture <div class="tooltip"><img class="info_image" src="image/info.svg" alt="Settings">
                    <span class="tooltiptext">
                        <h3>Water</h3>
                        <p>Dry: 0-35%</p>
                        <p>Wet: 35-100%</p>
                    </span>
                </div>
                <input type="text" name="moisture"></label>

            <label>Optimal light <div class="tooltip"><img class="info_image" src="image/info.svg" alt="Settings">
                    <span class="tooltiptext">
                        <h3>Light</h3>
                        <p>Shadow: 0-60%</p>
                        <p>Half-Shadow: 60-90%</p>
                        <p>Sunny: 90-100%</p>
                    </span>
                </div>

                <input type="text" name="optimal_light"></label>

            <label> Plant image <small>(.png, .jpg, .jpeg)</small>
                <input type="file" name="image" accept=".png,.jpg,.jpeg">
            </label>
            <?php if (isset($error)) { ?>
                <p class="error">
                    <?= htmlspecialchars($error); ?>
                </p>
            <?php } ?>
            <input type="submit" value="Create">
        </form>
    </main>
</body>

</html>