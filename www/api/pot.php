<?php 
require_once '../database.php';
header("content-type: application/json");

if (isset($_GET['nfc_id'])){

  $nfc_id = filter_input(INPUT_GET, 'nfc_id', FILTER_SANITIZE_SPECIAL_CHARS);
  
  $db = connect_db();

  $stmt = $db->prepare('SELECT * FROM pot
                               INNER JOIN plant
                               ON pot.plant_id = plant.id
                               WHERE pot.nfc_id LIKE :nfc_id');
                      
  $stmt->bindParam(':nfc_id', $nfc_id);
  
  if ($stmt->execute()) {
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) == 1){
      $currentRecord = $result[0];
      echo json_encode([
        'nfc_id' => htmlspecialchars(($currentRecord["nfc_id"])),
        'name' => htmlspecialchars(($currentRecord["name"])),
        'plant_id' => htmlspecialchars(($currentRecord["plant_id"])),
        'pot_id' => htmlspecialchars(($currentRecord["id"])),
        'moisture' => htmlspecialchars(($currentRecord["moisture"])),
        'optimal_light' => htmlspecialchars(($currentRecord["optimal_light"])),
        'image_path' => htmlspecialchars(($currentRecord["image_path"])),
        'is_inside' => htmlspecialchars(($currentRecord["is_inside"])),
      ]);
    } else if (count($result) == 0) {
        echo json_encode([
          'ERROR' => 'No record returned'
        ]);
    } 
  } else {
    echo json_encode("{'ERROR': 'unable to execute query!'}");
  }
}
