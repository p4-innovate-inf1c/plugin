<?php

// The user may only upload a file with a size under 3MB:
$max_file_size = 3;
if ($_FILES["image"]["size"] < $max_file_size * 1000 * 1000) {

    // The user may only upload .png, ,jpg or .jpeg files
    $acceptedFileTypes = ["image/jpg", "image/jpeg", "image/png"];

    // retrieve the MIME type of the uploaded file
    $uploadedFileType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $_FILES["image"]["tmp_name"]);

    // If the MIME type is in the array, proceed
    if (in_array($uploadedFileType, $acceptedFileTypes)) {
        $imageName = $_FILES["image"]["name"];
        if (strlen(trim($imageName)) <= 50) {
            if ($_FILES["image"]["error"] > 0) {
                $error = "Error: " . $_FILES["image"]["error"];
            } else {
                $extension = getExtension($uploadedFileType);
                if (!moveUploadedFile($_FILES["image"], $extension)) {
                    $error = "Error moving file";
                }
            }
        } else {
            $error = "Your file name contains to much characters! 50 or less characters are required to upload a file.";
        }
    } else {
        $error = "Invalid file type. Must be jpg, jpeg or png.";
    }
} else {
    $error = "Invalid file size. Must be less than 3MB.";
}


// Move an uploaded file to the correct map, based on MIME type
function moveUploadedFile($file, $extension)
{
    if (file_exists("image/" . $file["name"] . "." . $extension)) {
        exit("File " . htmlspecialchars($file["name"]) . " already exists!");
    } else {
        return move_uploaded_file($file["tmp_name"], "image/" . clean_filename($file["name"]));
    }
}

// retrieve the correct extension, based on MIME type
function getExtension($mimeType)
{
    $extension = "";
    switch ($mimeType) {
        case 'image/jpg':
            $extension = "jpg";
            break;
        case 'image/jpeg':
            $extension = "jpeg";
            break;
        case 'image/png':
            $extension = "png";
            break;
        default:
            $error = "No valid MIME type was found! Please get back to the main page!";
            break;
    }
    return $extension;
}