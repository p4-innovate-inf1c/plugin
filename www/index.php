<?php
function get_Credentials()
{
    if (file_exists('/etc/PowerPot/config.json')) {
        try {
            $filedata = file_get_contents('/etc/PowerPot/config.json');
        } catch (Exception) {
            die("Cant open file");
        }
        $json = json_decode($filedata);
        if (json_last_error() == 0) {
            return $json;
        }
        return null;
    }
}

$json = get_Credentials();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="resources/css/template.css" rel="stylesheet">
    <link href="resources/css/index.css" rel="stylesheet">
    <script src="js/mqtt31_paho.js"></script>
    <script>
        const MqttIp = "<?=$json->mqttIp?>"
        const Username = "<?=$json->username ?>"
        const Password = "<?=$json->password ?>"
    </script>
    <script src="js/mqtt.js"></script>
    <title>Platform overview</title>
</head>

<body>
<?php require_once 'header.php'; ?>
<h1 class="site_header">Platform overview</h1>
<div id='overview'>
    <h2>no connection</h2>
</div>
</body>

</html>
