<?php
require_once 'database.php';

$db = connect_db();

$stmt = $db->query('SELECT * FROM plant');
$plants = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (isset($_GET['delete'])) {
    $stmt = $db->prepare('DELETE FROM plant WHERE id = :id');
    $id = filter_input(INPUT_GET, 'delete', FILTER_SANITIZE_SPECIAL_CHARS);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $stmt = $db->prepare('DELETE FROM pot WHERE plant_id = :id');
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    if($stmt->execute()){
      $index = array_search($id, array_column($plants, "id"));
      $path = "image/".$plants[$index]["image_path"];
      if(file_exists($path)){
        unlink($path);
      }
    }
    header('Location: plant_overview.php');
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="resources/css/template.css" rel="stylesheet">
    <link href="resources/css/plant_overview.css" rel="stylesheet">
    <title>Plant management</title>
</head>

<body>
    <?php require_once 'header.php'; ?>

    <h1 class="site_header">Plant management</h1>
    <div class="overview">
        <div class="add-plant card">
            <a href="create_plant.php"><img src="image/add-button.png" alt="add button"></a>
        </div>
        <?php
        if (!empty($plants)) { ?>
            <?php foreach ($plants as $plant) {
           ?>
                <div class="card">
                    <div class="delete-button">
                        <a href="plant_overview.php?delete=<?= $plant["id"] ?>"
                        onclick="return confirm('Are you sure?');">❌</a>
                    </div>
                    <div class="plant-data">
                        <img src="image/<?= $plant["image_path"] ?>" alt="logo powerpot">
                        <p><b><?= $plant["name"] ?></b></p>
                        <p>Moisture: <?= $plant["moisture"] ?></p>
                        <p>Optimal light: <?= $plant["optimal_light"] ?></p>

                    </div>
                    <div class="edit-button">
                        <a href="manage_plant.php?id=<?= $plant["id"] ?>" class="button">Edit</a>
                    </div>
                </div>
            <?php } ?>
    </div>
<?php } ?>
</body>

</html>
