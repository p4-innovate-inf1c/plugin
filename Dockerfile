FROM trafex/php-nginx:2.6.0
EXPOSE 8080
# Change user to root to install packages
USER root
# Remove all files in document root && remove the nginx config (which caches everything)
RUN rm -rf /var/www/html/*
# Copy the custom NGINX config with cashing disabled for ingress
COPY nginx/nginx.conf /etc/nginx/nginx.conf
# Copy the custom php.ini to allow bigger files. 
COPY upload/php.ini /etc/php81/conf.d/php.ini
# Install pdo and fileinfo for PHP
RUN apk add php81-pdo_sqlite
RUN apk add php81-fileinfo
# Copy the PHP files to the docker container
COPY --chown=nobody www /var/www/html/
# Make a dir for the powerpot application
RUN mkdir /etc/PowerPot
RUN chown nobody:nobody /etc/PowerPot/
# Copy the db file and config to the /etc/PowerPot
COPY --chown=nobody powerpot.db /etc/PowerPot/powerpot.db
COPY --chown=nobody config.json /etc/PowerPot/config.json
# Create a dir the mqtt-manager
RUN mkdir /etc/PowerPot/mqtt-manager
# Copy the mqtt manager to the docker container
COPY python /etc/PowerPot/mqtt-manager/
# Install python3 and link it to python
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
# Install PIP
RUN python3 -m ensurepip
# Install the packages needed for the application
RUN pip3 install --upgrade pip wheel
RUN pip3 install -r /etc/PowerPot/mqtt-manager/requirements.txt
# Copy the custom entry point to run the application
COPY entrypoint.sh /etc/PowerPot/entrypoint.sh
CMD ["/bin/sh", "/etc/PowerPot/entrypoint.sh"]
# Change the user to nobody to run PHP
USER nobody
