#!/bin/sh
# Run the python mqtt-manager in the background
python /etc/PowerPot/mqtt-manager/s_platform.py &

# Run the original nginx tasks
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf